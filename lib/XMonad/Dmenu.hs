module XMonad.Dmenu (dmenuRun, dmenuRunM, dmenuYN, dmenuRunYN) where

import Control.Monad
import Control.Monad.IO.Class
import qualified Data.Map as M
import System.Exit
import System.Process

import XMonad

type ProcessError = (Int, String)


menu :: MonadIO m => String -> [String] -> [String] -> m (Either ProcessError String)
menu cmd args options = liftIO $ do
    (exitCode, sOut, sErr) <- readProcessWithExitCode cmd args $ unlines options
    return $ case exitCode of
               ExitSuccess -> Right $ filter (/='\n') sOut
               ExitFailure i -> Left (i, sErr)


dmenu :: MonadIO m => String -> [String] -> m (Either ProcessError String)
dmenu prompt = menu "dmenu" ["-i", "-p", prompt]

dmenuYN :: MonadIO m => String -> m Bool
dmenuYN prompt = do
    yn <- dmenu prompt []
    return $ case yn of
             Right _ -> True
             Left _ -> False

-- dmenuRun :: MonadIO m => String -> [(String, m ())] -> m ()
dmenuRun :: String -> [(String, X ())] -> X ()
dmenuRun prompt options = do
    sel <- dmenu prompt $ fst <$> options
    case sel of
      Right sel' -> maybe (return ()) id $ lookup sel' options
      Left _ -> return ()


-- dmenuRunM :: MonadIO m => String -> M.Map String (m ()) -> m ()
dmenuRunM :: String -> M.Map String (X ()) -> X ()
dmenuRunM prompt options = do
    sel <- dmenu prompt $ M.keys options
    case sel of
      Right sel' -> maybe (return ()) id $ M.lookup sel' options
      Left _ -> return ()


-- dmenuRunYN :: MonadIO m => String -> m () -> m ()
dmenuRunYN :: String -> X () -> X ()
dmenuRunYN prompt action = do
    yn <- dmenuYN prompt
    case yn of
      True -> action
      False -> return ()
