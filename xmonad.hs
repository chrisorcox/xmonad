--  __  ____  __                       _
--  \ \/ /  \/  | ___  _ __   __ _  __| |
--   \  /| |\/| |/ _ \| '_ \ / _` |/ _` |
--   /  \| |  | | (_) | | | | (_| | (_| |
--  /_/\_\_|  |_|\___/|_| |_|\__,_|\__,_|

-- Modules {{{
import Control.Monad (liftM, forM, forM_)
import Data.List
import qualified Data.Map as M
import Data.Maybe
import System.Exit
import System.IO

import XMonad hiding ( (|||) )          -- we'll get ||| from X.L.LayoutCombinators
import qualified XMonad.StackSet as W

import qualified XMonad.Dmenu as D

-- Actions
import XMonad.Actions.Commands          -- for xmonadctl
import XMonad.Actions.ConditionalKeys
import XMonad.Actions.CopyWindow
import XMonad.Actions.CycleSelectedLayouts
import XMonad.Actions.CycleWS
import XMonad.Actions.DynamicProjects
import XMonad.Actions.DynamicWorkspaces -- withNthWorkspace
import XMonad.Actions.FocusNth
import XMonad.Actions.Navigation2D
import XMonad.Actions.SelectWindow      -- easymotion!
import XMonad.Actions.SpawnOn
import XMonad.Actions.WindowBringer
import XMonad.Actions.WithAll
-- Hooks
import XMonad.Hooks.DynamicLog          -- for xmobar
import XMonad.Hooks.EwmhDesktops
import XMonad.Hooks.InsertPosition      -- where to place newly spawned windows
import XMonad.Hooks.ManageDocks         -- to avoid xmobar
import XMonad.Hooks.ManageHelpers
import XMonad.Hooks.ServerMode          -- xmonadctl
import XMonad.Hooks.SetWMName           -- for java programs
import XMonad.Hooks.UrgencyHook (withUrgencyHook, NoUrgencyHook(..))

-- Layouts
import XMonad.Layout.Accordion
import XMonad.Layout.Circle
import XMonad.Layout.Dishes
import XMonad.Layout.StackTile          -- resizable dishes
import XMonad.Layout.Fullscreen
import XMonad.Layout.Column
import XMonad.Layout.Grid
import XMonad.Layout.IndependentScreens (countScreens)  -- for multiple xmobars
import XMonad.Layout.LayoutCombinators
import XMonad.Layout.LayoutBuilder      -- for building my own layouts
import XMonad.Layout.LayoutModifier (ModifiedLayout)
import XMonad.Layout.MultiToggle
import XMonad.Layout.MultiToggle.Instances
import XMonad.Layout.OneBig
import XMonad.Layout.PerWorkspace
import XMonad.Layout.Renamed            -- rename layouts
import XMonad.Layout.ResizableTile
import XMonad.Layout.Simplest
import XMonad.Layout.SubLayouts
import XMonad.Layout.Tabbed
import XMonad.Layout.ThreeColumns
import XMonad.Layout.WindowNavigation   -- necessary for sublayouts

-- Utils
import XMonad.Util.Cursor
import qualified XMonad.Util.Dmenu as D
import XMonad.Util.EZConfig
import XMonad.Util.Run (runProcessWithInput, spawnPipe, safeSpawn)
import XMonad.Util.SpawnOnce
import XMonad.Util.Ungrab               -- for dmenu prompts

import XMonad.Prompt
import XMonad.Prompt.XMonad
import XMonad.Prompt.ConfirmPrompt


-- }}}
-- Main {{{

main = do
    -- xmproc <- spawnPipe myStatusBar
    nScreens <- countScreens
    xmproc <- forM [0..nScreens - 1] (spawnPipe . statusOnScreen)
    xmonad . withUrgencyHook NoUrgencyHook
           . dynamicProjects projects
           . withNavigation2DConfig myNav2DConf
           . ewmh
           $ myConfig xmproc

myConfig x = def { keys = flip mkKeymap myKeys
                 , terminal = "term"
                 , modMask = myModMask
                 , clickJustFocuses = False
                 , focusFollowsMouse = False
                 , normalBorderColor = inactiveCol
                 , focusedBorderColor = activeCol
                 , borderWidth = myBorder
                 , workspaces = myWorkspaces
                 , layoutHook = myLayoutHook
                 , logHook = myLogHook x
                 , manageHook = myManageHook
                 , handleEventHook = myHandleEventHook
                 , startupHook = myStartupHook
                 }
                 `additionalMouseBindings`
                 myExtraMouse

-- myStatusBar = "xmobar -x 0 ~/.xmonad/xmobar/xmobar.hs"
-- TODO: have this check to see if there's an alternative xmobar config for alt screens
-- Also, look into XMonad.Hooks.DynamicBars
statusOnScreen :: Int -> String
statusOnScreen i = "xmobar -x " ++ show i ++ " ~/.xmonad/xmobar/xmobar.hs"
-- statusOnScreen i = "~/.xmonad/xmobar/xmobar -x " ++ show i
--- }}}
-- Workspaces {{{

ws1 = "main"
ws2 = "www"
ws3 = "TeX"
ws4 = "code"
ws5 = "aux"
ws6 = "chat"
ws7 = "media"
ws8 = "mail"
ws9 = "sys"

myWorkspaces :: [String]
myWorkspaces = [ws1, ws2, ws3, ws4, ws5, ws6, ws7, ws8, ws9]



projects :: [Project]
projects = [ Project { projectName = "mail"
                     , projectDirectory = "~/"
                     , projectStartHook = Just $ do
                         runInTerm "mutt"
                         runInTerm "ikhal"
                     }
           ]

-- }}}
-- Theme {{{

bGLighter = "#393649"
bGLight = "#2e2b3b"
bG = "#22212C"
bGDark = "#17161d"
bGDarker = "#0b0b0f"

comment = "#7970a9"
selection = "#454158"
subtle = "#424450"

fG = "#f8f8f2"

cyan = "#80ffea"
green = "#8aff80"
orange = "#ffca80"
pink = "#ff80bf"
purple = "#9580ff"
red = "#ff5555"
yellow = "#ffff80"

activeCol = pink
inactiveCol = bGDarker


myFontSmall = "xft:monospace:style=Regular:size=7:antialias=true:hinting=true"
myFont = "xft:monospace:style=Regular:size=9:antialias=true:hinting=true"
myFontBig = "xft:monospace:style=Regular:size=11:antialias=true:hinting=true"

myBorder = 2


myTabTheme = def { fontName = myFont
                 , activeColor = activeCol
                 , inactiveColor = bG
                 , activeBorderColor = activeCol
                 , inactiveBorderColor = inactiveCol
                 , activeTextColor = bGDarker
                 , inactiveTextColor = fG
                 }

defPrompt :: XPConfig
defPrompt = def { font = myFont
                , bgColor = yellow
                , fgColor = bGDarker
                , fgHLight = yellow
                , bgHLight = bGDarker
                , borderColor = bGDarker
                , promptBorderWidth = 1
                , position = Top
                , alwaysHighlight = True
                }

wsPrompt :: XPConfig
wsPrompt = defPrompt



-- }}}
-- Keys {{{

myModMask = mod4Mask

-- helpers
zipKeys :: String -> [String] -> [a] -> (a -> X ()) -> [(String, X ())]
zipKeys m ks as f = zipWith (\k a -> (m ++ k, f a)) ks as

zipKeys' :: [String] -> [String] -> [a] -> [a -> X ()] -> [(String, X ())]
zipKeys' ms ks as fs = concat $ (\(m, f) -> zipKeys m ks as f) <$> zip ms fs

addPrfx :: String -> [(String, X ())] -> [(String, X ())]
addPrfx prfx = map (\(s, x) -> (prfx ++ " " ++ s, x))

hjklKeys = ["h", "j", "k", "l"]
arrowKeys = ["<L>", "<D>", "<U>", "<R>"]
dir2DKeys = [L, D, U, R]
wKeys = show <$> [1..9] -- ++ [0]

-- keys
myKeys, appKeys, winKeys, navKeys, wsKeys, loKeys, ctrlKeys :: [(String, X ())]
myKeys = appKeys ++ winKeys ++ loKeys ++ ctrlKeys ++ navKeys ++ wsKeys

appKeys = [ ("M-<Space>", safeSpawn launcher [])
          , ("M-<Return>", safeSpawn term [])
          , ("M-S-<Return>", safeSpawn altTerm [])
          , ("M-\\", safeSpawn browser [])
          , ("M-S-\\", safeSpawn altBrowser [])
          ]

winKeys = [ ("M-<Backspace>", kill1)
          , ("M-C-<Backspace>", confirmPrompt defPrompt "Kill all windows?" killAll)
          , ("M-S-<Backspace>", kill)
          , ("M-/", gotoMenuConfig myWGConf)
          , ("M-S-/", bringMenuConfig myWBConf)
          ]
       ++ (zipKeys "M-y " wKeys myWorkspaces (windows . copy))
       ++ addPrfx "M-y" [ ("0", windows copyToAll)
                        , ("<Backspace>", killAllOtherCopies)
                        ]


loKeys = [ ("M-v", (return laySels) >>= prompt "Layouts")
         , ("M-t", sendMessage $ JumpToLayout "mStack")
         , ("M-m", cycleThroughLayouts ["monocle", "mStack"])
         , ("M-d", cycleThroughLayouts ["deck", "mStack"])
         , ("M-S-t", cycleThroughLayouts ["mTabs", "mStack"])
         , ("M1-<Return>", windows W.swapMaster)
         , ("M-=", sendMessage $ IncMasterN 1)
         , ("M--", sendMessage $ IncMasterN (-1))
         , ("M-b", sendMessage ToggleStruts)
         , ("M-r", sendMessage $ Toggle MIRROR)
         , ("M-[", sendMessage Shrink)
         , ("M-]", sendMessage Expand)
         , ("M-S-]", sendMessage MirrorShrink)
         , ("M-S-[", sendMessage MirrorExpand)
         , ("M-C-[", asks (XMonad.layoutHook . config) >>= setLayout)
         , ("M-C-]", asks (XMonad.layoutHook . config) >>= setLayout)
         , ("M-S-=", sequence_ [ withFocused $ windows . W.sink, sendMessage $ Toggle NBFULL ])
         ]
      ++ (zipKeys' ["M-C-", "M-w "] (hjklKeys ++ arrowKeys) (cycle dir2DKeys) $ repeat (sendMessage . pullGroup))
      ++ addPrfx "M-w" [ ("u", withFocused $ sendMessage . UnMerge)
                       , ("m", withFocused $ sendMessage . MergeAll)
                       , ("<Tab>", toSubl NextLayout)
                       , ("f", withFocused toggleFloat)
                       , ("S-f", sinkAll)
                       ]

wsKeys = [ ("M-`", toggleWS)
         , ("M-n", moveTo Next HiddenWS)
         , ("M-S-n", moveTo Prev HiddenWS)
         ]
      ++ addPrfx "M-'" [ ("'", selectWorkspace wsPrompt)
                       , ("M-'", selectWorkspace wsPrompt)
                       , ("n", appendWorkspacePrompt wsPrompt)
                       , ("a", appendWorkspacePrompt wsPrompt)
                       , ("<Backspace>", removeEmptyWorkspace)
                       , ("r", renameWorkspace wsPrompt)
                       , ("m", withWorkspace wsPrompt (windows . W.shift))
                       ]
      ++ (zipKeys' ["M-", "M-S-"] wKeys [0..] $ withNthWorkspace <$> [W.greedyView, W.shift])

navKeys = [ ("M-S-f", switchLayer)      -- navigation2D
          , ("M-i", bindOn LD $ altAct1 ["tabs", "mTabs", "deck"] (windows W.focusDown) (onGroup W.focusDown') )
          , ("M-u", bindOn LD $ altAct1 ["tabs", "mTabs", "deck"] (windows W.focusUp) (onGroup W.focusUp') )
          , ("M-S-i", windows W.swapDown)
          , ("M-S-u", windows W.swapUp)
          , ("M-g g", windows W.focusMaster)
          , ("M-g M-g", windows W.focusMaster)
          , ("M-f", selectWindowColors bGDarker yellow >>= (flip whenJust (windows . W.focusWindow)))
          ]
       ++ (zipKeys' ["M-", "M-S-"] (hjklKeys ++ arrowKeys) (cycle dir2DKeys) $ (\f -> flip f True) <$> [windowGo, windowSwap])
       ++ (zipKeys' ["M-", "M-S-", "M-C-"] [",", "."] [L, R] $ (\f -> flip f True) <$> [screenGo, windowToScreen, screenSwap])
       ++ (zipKeys' ["M-g ", "M-g S-"] (show <$> [0..9]) [0..9] [focusNth, swapNth])

ctrlKeys = [ ("M-S-c", myCommands >>= prompt "XMonad")
           ]
          ++ addPrfx "M-x" [ ("c", myCommands >>= prompt "XMonad")
                         , ("q", confirmPrompt defPrompt "Quit XMonad?" . io $ exitWith ExitSuccess)
                         , ("r", spawn "xmonad --restart")
                         , ("S-r", spawn "xmonad --recompile && xmonad --restart")
                         ]



toggleFloat :: Window -> X ()
toggleFloat w = windows (\s -> if M.member w (W.floating s)
                                  then W.sink w s
                                  else W.float w (W.RationalRect (1/3) (1/4) (1/2) (4/5)) s )

altAct :: [[String]] -> [X ()] -> X () -> [(String, X ())]
altAct xss acts defact = (concat $ (\(xs, a) -> zip xs $ repeat a) <$> zip xss acts ) ++ ("", defact) : []

altAct1 :: [String] -> X () -> X () -> [(String, X ())]
altAct1 xs act defact = altAct [xs] [act] defact


wsSel :: [(String, X ())]
wsSel = [ (fst s, withNthWorkspace W.greedyView $ snd s) | s <- zip myWorkspaces [0..] ]


myExtraMouse :: [((ButtonMask, Button), Window -> X ())]
myExtraMouse = [ ((myModMask, 4), (\_ -> moveTo Prev HiddenWS))
               , ((myModMask, 5), (\_ -> moveTo Next HiddenWS))
               ]


-- }}}
-- Layouts {{{


-- Tell Navigation2D how to handle specific layouts
myNav2DConf :: Navigation2DConfig
myNav2DConf = def { defaultTiledNavigation = hybridOf sideNavigation centerNavigation
                  , floatNavigation        = hybridOf lineNavigation centerNavigation
                  , screenNavigation       = lineNavigation
                  , layoutNavigation       = [ ("Full", centerNavigation)
                                             , ("monocle", centerNavigation)
                                             , ("dishes", hybridOf sideNavigation centerNavigation)
                                             ]
                  , unmappedWindowRect     = [ ("Full", singleWindowRect)
                                             , ("monocle", singleWindowRect)
                                             ]
                  }

mStackWS' = ["TeX", "code", "mail"]

myLayoutHook = mkToggle (single NBFULL) -- fullscreen
             . onWorkspaces mStackWS' wideMStack
             $ mStack ||| deck ||| mTabs ||| sharedLayouts
                 where
                     wideMStack    = mStack' ||| deck' ||| mTabs' ||| sharedLayouts
                     sharedLayouts = monocle ||| grid ||| threeColumn ||| oneColumn ||| tabs ||| dishes ||| accordion ||| oneBig ||| circle

laySels :: [(String, X ())]
laySels = ("reset", asks (XMonad.layoutHook . config) >>= setLayout) : [ (s, sendMessage $ JumpToLayout s) | s <- l ]
    where
        l = [ "mStack"
            , "monocle"
            , "deck"
            , "grid"
            , "threeColumn"
            , "oneColumn"
            , "tabs"
            , "dishes"
            , "accordion"
            , "oneBig"
            , "circle"
            , "mTabs"
            ]


named :: LayoutClass l a => String -> l a -> ModifiedLayout Rename l a
named n = renamed [(XMonad.Layout.Renamed.Replace n)]



-- each of these neads to include avoidStruts since we want fullscreen to cover the bar
-- also, we need to put MIRROR after avoidStruts due to function composition order
mStack =
    named "mStack"
  . avoidStruts
  . mkToggle1 MIRROR
  . configurableNavigation noNavigateBorders
  . addTabs shrinkText myTabTheme
  . subLayout [] (Simplest ||| Accordion)
  $ ResizableTall 1 (3/100) (1/2) []

mStack' =
    named "mStack"
  . avoidStruts
  . mkToggle1 MIRROR
  . configurableNavigation noNavigateBorders
  . addTabs shrinkText myTabTheme
  . subLayout [] (Simplest ||| Accordion)
  $ ResizableTall 1 (3/100) (2/3) []

threeColumn =
    named "threeColumn"
  . avoidStruts
  . configurableNavigation noNavigateBorders
  . addTabs shrinkText myTabTheme
  . subLayout [] (Simplest ||| Accordion)
  $ ThreeColMid 1 (3/100) (1/2)

oneBig =
    named "oneBig"
  . avoidStruts
  . mkToggle1 MIRROR
  . configurableNavigation noNavigateBorders
  . addTabs shrinkText myTabTheme
  . subLayout [] (Simplest ||| Accordion)
  $ OneBig (2/3) (2/3)

tabs      = named "tabs"      . avoidStruts . addTabs shrinkText myTabTheme $ Simplest
oneColumn = named "oneColumn" . avoidStruts . mkToggle1 MIRROR              $ Column 1.1
dishes    = named "dishes"    . avoidStruts . mkToggle1 MIRROR              $ StackTile 2 (3/100) (5/6)
accordion = named "accordion" . avoidStruts . mkToggle1 MIRROR              $ Accordion
grid      = named "grid"      . avoidStruts . mkToggle1 MIRROR              $ Grid
circle    = named "circle"    . avoidStruts $ Circle
monocle   = named "monocle"   . avoidStruts $ Full

mTabs =
    named "mTabs"
  . avoidStruts
  . layoutN 1 (relBox 0 0 (1/2) 1) (Just $ relBox 0 0 1 1) Full
  $ layoutAll (relBox (1/2) 0 1 1) (tabbed shrinkText myTabTheme)

mTabs' =
    named "mTabs"
  . avoidStruts
  . layoutN 1 (relBox 0 0 (2/3) 1) (Just $ relBox 0 0 1 1) Full
  $ layoutAll (relBox (2/3) 0 1 1) (tabbed shrinkText myTabTheme)

deck =
    named "deck"
  . avoidStruts
  . layoutN 1 (relBox 0 0 (1/2) 1) (Just $ relBox 0 0 1 1) Full
  $ layoutAll (relBox (1/2) 0 1 1) Full

deck' =
    named "deck"
  . avoidStruts
  . layoutN 1 (relBox 0 0 (2/3) 1) (Just $ relBox 0 0 1 1) Full
  $ layoutAll (relBox (2/3) 0 1 1) Full

-- }}}
-- Log {{{

-- here the Handle is an xmobar process
myLogHook :: [Handle] -> X ()
myLogHook hs = do
    copies <- wsContainingCopies
    let wrapIt l ws = padl l . (if ws `elem` copies then padr "^" else padr " ") $ mkClickable ws
    ewmhDesktopsLogHook
    dynamicLogWithPP $ def { ppTitle            = altBG yellow . pad . shorten 80 . xmobarStrip
                           , ppCurrent          = altBG yellow . wrapIt " "
                           , ppVisible          = altBG orange . wrapIt "*"
                           , ppVisibleNoWindows = Just $ altBG orange . wrapIt " "
                           , ppHidden           = wrapIt "*"
                           , ppHiddenNoWindows  = wrapIt " "
                           , ppUrgent           = altBG red . wrapIt "!"
                           , ppSep              = " "
                           , ppWsSep            = "<icon=separators/wsseparator.xpm/>"
                           , ppLayout           = wrap "<icon=layouts/" ".xpm/>"
                           , ppOrder            = \(ws:l:t:ex) -> ws : l : ex ++ [t]
                           , ppOutput           = \str -> forM_ hs (flip hPutStrLn str)
                           , ppExtras           = windowCount : []
                           }
                               where
                                   windowCount = gets $ Just . show . length . W.integrate' . W.stack . W.workspace . W.current . windowset
                                   -- mkClickable = clickable (zip myWorkspaces [1..])
                                   mkClickable = clickable'
                                   altBG = xmobarColor bGDarker
                                   padl x = wrap x ""
                                   padr x = wrap "" x

-- This is a hack to allow clicking on the workspace to switch
clickable :: [(WorkspaceId, Int)] -> WorkspaceId -> String
clickable ws w = fromMaybe w $ (\x -> xmobarAction ("xdotool key super+" ++ show x) "1" w) <$> lookup w ws

-- This is a hack using xmonadctl instead
clickable' :: WorkspaceId -> String
clickable' w = xmobarAction ("xmonadctl view\\\"" ++ w ++ "\\\"") "1" w


-- }}}
-- Startup {{{

myStartupHook :: X ()
myStartupHook = do
    setWMName "LG3D"
    setDefaultCursor xC_left_ptr
    spawnOnce "polkit.start"
    spawnOnce "tray.start"
    spawnOnce "power-manager.start"
    spawnOnce "network-manager.start"
    spawnOnce "disk-mount.start"
    spawnOnce "bluetooth.start"
    spawnOnce "notifications.start"
    spawnOnce "locker.start"
    spawnOnce "bg.start"
    spawnOnce "sxhkd.start"


-- }}}
-- Actions {{{

myManageHook :: ManageHook
myManageHook = manageSpecific
           <+> manageOneSpecific
           <+> manageDocks
           <+> fullscreenManageHook
           <+> manageSpawn
               where
                   manageSpecific = composeAll . concat $
                       -- [ [ isDialog <&&> className =? w --> doCenterFloat | w <- myWWW ]
                       [ [ className =? w --> doShift "www"   | w <- myWWW ]
                       , [ className =? c --> doShift "chat"  | c <- myChat ]
                       , [ className =? m --> doShift "media" | m <- myMedia ]
                       , [ className =? f --> doCenterFloat   | f <- myFloats ]
                       ]
                   manageOneSpecific = composeOne
                       [ transience
                       , resource =? "desktop_window"         -?> doIgnore
                       , isWinRole =? "GtkFileChooserDialog"  -?> doCenterFloat
                       , isWinRole =? "pop-up"                -?> doCenterFloat
                       , isSkipTask <&&> className =? "Skype" -?> insertPosition Below Older -- prevent skype popup window from taking focus
                       , isSplash                             -?> doCenterFloat
                       , isDialog                             -?> doCenterFloat
                       , isFullscreen                         -?> doFullFloat
                       , pure True                            -?> insertPosition Below Newer -- place new windows right below the current one and focus it
                       ]
                   isSplash = isInProperty "_NET_WM_WINDOW_TYPE" "_NET_WM_WINDOW_TYPE_SPLASH"
                   isSkipTask = isInProperty "_NET_WM_STATE" "_NET_WM_STATE_SKIP_TASKBAR"
                   isWinRole  = stringProperty "WM_WINDOW_ROLE"
                   myWWW = [ "browser", "qutebrowser", "Firefox", "Vivaldi-stable", "brave-browser" ]
                   myChat = [ "Skype", "zoom" ]
                   myMedia = [ "vlc", "mpv", "Kodi" ]
                   myFloats = [ "Pavucontrol" ]



-- myHandleEventHook :: Event -> X Data.Semigroup.Internal.All
myHandleEventHook = docksEventHook
                <+> serverModeEventHookCmd' myCommands -- for xmonadctl
                <+> handleEventHook def
                <+> XMonad.Layout.Fullscreen.fullscreenEventHook -- constrain fullscreen to a window


myCommands :: X [(String, X ())]
myCommands = (++ (extraCMDs ++ screenCommands)) <$> workspaceCommands
        where extraCMDs = [ ("kill"         , kill)
                          , ("killall"      , killAll)
                          , ("master-focus" , windows W.focusMaster)
                          , ("master-swap"  , windows W.swapMaster)
                          , ("master-incr"  , sendMessage $ IncMasterN 1)
                          , ("master-decr"  , sendMessage $ IncMasterN (-1))
                          , ("sink"         , withFocused $ windows . W.sink)
                          , ("sinkall"      , sinkAll)
                          , ("float-toggle" , withFocused toggleFloat)
                          , ("shrink"       , sendMessage Shrink)
                          , ("expand"       , sendMessage Expand)
                          , ("quit"         , io $ exitWith ExitSuccess)
                          , ("restart"      , spawn "xmonad --restart")
                          , ("reset-layout" , asks (XMonad.layoutHook . config) >>= setLayout)
                          , ("layout"       , (return laySels) >>= prompt "Layouts")
                          ]


-- }}}
-- Prompts {{{

myDmenu :: MonadIO m => String -> [String] -> m String
myDmenu p opts = D.menuArgs "dmenu" ["-i", "-p", p] opts

prompt :: String -> [(String, X ())] -> X ()
prompt p commands = do
    sel <- myDmenu p $ fst <$> commands
    maybe (pure ()) id $ lookup sel commands

-- testPrompt :: String -> X () -> X ()
-- testPrompt p a = liftIO $ do
    -- yn <- D.dmenuYN p
    -- case yn of
      -- True -> a
      -- False -> pure ()

-- prompt :: XPrompt p => p -> XPConfig -> [(String, X ())] -> X ()
-- prompt p cfg commands = mkXPrompt p cfg (mkComplFunFromList' $ map fst commands) $ fromMaybe (return ()) . (`lookup` commands)

-- data CmdPrompt = CmdPrompt
-- instance XPrompt CmdPrompt where
    -- showXPrompt CmdPrompt = "XMonad: "

-- data LayoutPrompt = LayoutPrompt
-- instance XPrompt LayoutPrompt where
    -- showXPrompt LayoutPrompt = "Layout: "

myWBConf :: WindowBringerConfig
myWBConf = def { menuArgs = ["-i", "-l", "20", "-p", "Bring Window"] }

myWGConf :: WindowBringerConfig
myWGConf = def { menuArgs = ["-i", "-l", "20", "-p", "GoTo Window"] }

-- }}}
-- Applications {{{

term = "term"
altTerm = "alt_term"
launcher = "dmenu-run"
browser = "browser"
altBrowser = "alt_browser"

runInTerm :: String -> X ()
runInTerm app = safeSpawn term ["-e", app]


-- }}}
