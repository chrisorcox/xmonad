#!/bin/sh

icon="sunny"

# weathercodes can be found at http://www.worldweatheronline.com/feed/wwoConditionCodes.txt
case $(parseweather weatherCode) in
    113 ) icon="sunny" ;;
    116 ) icon="partly_cloudy" ;;
    119 ) icon="mostly_cloudy" ;;
    122|143|248|260 ) icon="cloudy" ;;
    176|263|266|281|293|296|311|317|353|362 ) icon="light_rain" ;;
    179|182|185|323|326|368|374 ) icon="light_snow" ;;
    200|386|389|392|395 ) icon="thunder" ;;
    227|329|332|230|335|338|350|371|377 ) icon="snow" ;;
    284|299|302|305|308|314|320|356|359|365 ) icon="rain" ;;
esac

#case "$weather" in
    #*[Uu]nknown*|*[Ee]rror* )
        #weather="ERROR" ;;
#esac


printf '<action=`term -e weather` button=1>'
printf '<icon=weather/%s.xbm/></action> ' "$icon"
printf '%s°F %s' "$(parseweather temp)" "$(parseweather description)"
#printf '%s°F %s' "$(weather --noup temp_F)" "$(weather --noup weatherDesc)"
