#!/bin/sh

vol=$(amixer get Master | grep Playback | awk -F'[]%[]' 'END{print $2}')

#printbar() {
	#printf '%*s' "$1" | tr ' ' "$2"
#}

range=$((vol/25))

case "$range" in
    0 ) icon="0" ;;
    1 ) icon="1" ;;
    2 ) icon="2" ;;
    3|4 ) icon="3" ;;
esac
#vol_icon="3"
#case "$vol" in
	#[0-9]|1[0-9]|2[0-4] )
		#vol_icon="0" ;;
	#2[5-9]|[3-4][0-9] )
		#vol_icon="1" ;;
	#[5-6][0-9]|7[0-4] )
		#vol_icon="2" ;;
#esac

amixer get Master | grep -q '\[off\]' && icon="mute" 


printf '<action=`pavucontrol` button=1>'
printf "<icon=volume/volume_%s.xpm/></action>%s%%" "$icon" "$vol"
